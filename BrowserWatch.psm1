function Start-BrowserWatch($url){
   #clean up any previous event subscriptions
   Stop-BrowserWatch
   
   $webDriverPath = join-path $psScriptRoot "WebDriver.dll"
   [void][system.reflection.assembly]::LoadFrom($webDriverPath)

   $chromeOpts = new-object OpenQA.Selenium.Chrome.ChromeOptions
   $chromeOpts.AddArgument("--disable-application-cache")
   $driver = new-object OpenQA.Selenium.Chrome.ChromeDriver
   
   if($url){
      $driver.Url = $url
      $driver.Navigate()
   }

   $watcher = new-object System.IO.FileSystemWatcher
   $watchPath = get-location
   $watcher.Path = $watchPath
   $watcher.IncludeSubdirectories = $true
   $watcher.EnableRaisingEvents = $true
   
   # handle the file modification and reload the browser
   set-variable -Name nextEventDate -Value (get-date).AddSeconds(2) -Scope Script
   $changed = Register-ObjectEvent -InputObject $watcher -EventName Changed -SourceIdentifier Changed -MessageData $driver -Action {
      $eventDate = get-date
      if($eventDate -gt $nextEventDate){
         set-variable -Name nextEventDate -Value (get-date).AddSeconds(2) -Scope Script
         write-host "$eventDate Changed: $($eventArgs.FullPath)"
         $event.MessageData.Navigate().Refresh()
      }
   }   
 
   write-host -foregroundColor Green "Watching '$watchPath'"
   write-host "Type <Ctrl-C> to shutdowen Chrome."
   write-host "Run 'Stop-BrowserWatch' to unregisted file watcher events."
}

function Stop-BrowserWatch{
    Get-EventSubscriber | foreach-object {
      Unregister-Event $_.SubscriptionId
   }
}