$currentDir = split-path $MyInvocation.MyCommand.Definition
$userPath = $env:PSModulePath.split(";")[0]
$modulePath = Join-Path $userPath "BrowserWatch"
$zipPath = Join-Path $currentDir "browserwatch-support.zip" 

function Copy-Module{
   write-host "Copying module file."
   if(-not(Test-Path $modulePath)){
      New-Item -Path $modulePath -ItemType Directory | Out-Null
   }
  
   Copy-Item -Path BrowserWatch.psm1 -Destination $modulePath
}

function Download-Dependencies{
    write-host "Downloading WebDriver dependencies."
    $url = "https://bitbucket.org/farmas/browser-watch/downloads/browser-watch-support.zip"
    
    $client = new-object System.Net.WebClient
    $client.DownloadFile($url, $zipPath)
}

function Unzip-Dependencies{
    write-host "Unzipping WebDriver dependencies."
    $shell = new-object -com shell.application
    $zipPackage = $shell.Namespace($zipPath)
    $destination = $shell.Namespace($modulePath)

    $destination.CopyHere($zipPackage.Items(), 20)
}

Copy-Module
Download-Dependencies
Unzip-Dependencies

write-host -foregroundColor Green "Module installed. Type 'Import-Module BrowserWatch' to import."