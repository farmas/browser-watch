#Overview
BrowserWatch is a powershell module that can auto refresh the browser whenver a file changes in a directory. It uses a FileSystemWatcher to check for changes in your working directory and WebDriver to control the Chrome browser

#Usage
1. **Download**: Clone or download the repo.

2. **Install**: In a powershell window, run install.ps1. This will copy the module in your local profile and unpack the webdriver dependencies into it.

3. **Import**: Run “Import-Module BrowserWatch” to load it to your current session.

4. **Start**: In your working directory run “Start-BrowserWatch”. This will start watching for file changes in this directory and bring up Chrome.

5. **Make changes and watch the magic.**